const pgp = require('pg-promise')();

pgp.pg.defaults.ssl = true

const db = pgp(process.env.MAIN_PIRAYA_DB_URL)

module.exports = {
  pg: db,
  getAllBings: function(callback) {
    db.any("select * from bing JOIN users ON (bing.uid = users.uid)")
    .then(function(data) {
      //Success
      console.log("Successfully fetched bing!")
      callback(data);
    })
    .catch(function(error) {
      console.log("Something went wrong when feting all bings!");
      console.log("ERROR:", error.message || error);
      callback(null);
    });
  },

  getUser: function(uid, callback) {
  	db.oneOrNone("select uid from users where uid=$1", [uid])
      .then(function(data) {

        if (data === null){
			console.log("Could not find user in team piraya")
      callback(null);
        }else{
			callback(data.uid);
        }

        //Success
        console.log("Successfully fethched user with uid " + uid)
      })
      .catch(function(error) {
        console.log("Something went wrong when selecting user!\n");
        console.log("ERROR:", error.message || error);
      });

  },

  getUserBings: function(uid, callback) {
    db.any("select * from bing where uid=$1", [uid])
       .then(function(data) {
        //Success
        callback(data);
        console.log("Successfully fethched user with uid " + uid);
      })
      .catch(function(error) {
        console.log("Something went wrong when getting user bings!\n");
        console.log("ERROR:", error.message || error);
      });


  }
      
};