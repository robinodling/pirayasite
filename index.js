
"use strict";

var express = require('express');
var app = express();

var request = require('request');

var flash = require('connect-flash');

var session = require('express-session');
var pgSession = require('connect-pg-simple')(session);

var passport = require('passport');
var SlackStrategy = require('passport-slack').Strategy;

var dbutil = require('./src/dbutil.js')

passport.use(new SlackStrategy({
    clientID: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
	callbackURL: process.env.CALLBACK_URL,
	team: process.env.TEAM_ID,
	scope: 'identify',
	extendedUserProfile: false
  },
  function(accessToken, refreshToken, profile, done) {

  	return done(null, profile);

  }
));


var sess = {
	store: new pgSession({
    	conString: process.env.MAIN_PIRAYA_DB_URL
  	}),
	resave: true,
	saveUninitialized: true,
	secret: process.env.COOKIE_SECRET,
	cookie: { 
		maxAge: 1 * 24 * 60 * 60 * 1000,
		secure: false
	}
}

if (process.env.NODE_ENV === 'production') {
  app.set('trust proxy', 1) // trust first proxy
  sess.cookie.secure = true // serve secure cookies
}


app.use(session(sess));
app.use(flash());
/*
app.use(function (req, res, next) {
  

});
*/

app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {

	dbutil.getAllBings(function(data) {
		let name = request.session['name'] 
		response.render('pages/index', {results: data, name: name});

	});

});

app.get('/logout', function(request, response) {

	    request.session.destroy();
    	
    	response.render('pages/index', {
    		message: 'You succesfully removed your used data!'
    	})

});

app.get('/auth/slack',function(request, response, next) {
	
	let uid = request.session['uid']

	if(uid) {
		response.redirect('/user/bing')
	} else{
		next();
	}

},
  
  passport.authorize('slack')
);

app.get('/user/bing', function(request, response) {


	let uid = request.session['uid']

    console.log("Getting bings for user: " + uid)
	
	dbutil.getUserBings(uid, function(data) {

    	if(data === null){
    		let name = request.session['name']
    		response.render('pages/user_bing', {name: name, bings: data});
    	
    	} else {
    		let name = request.session['name']
    		console.log("NAME: " + name)
    		response.render('pages/user_bing', {name: name, bings: data});
    	}

  		

  	});
});

app.get('/auth/slack/callback', 
  passport.authorize('slack', { failureRedirect: '/' }),
  function(req, res) {
    // Successful authentication, redirect home.

    if(req.account._json.team_id !== process.env.TEAM_ID){
    	req.session.destroy();
    	res.render('pages/index', {
    		message: 'You tried to authorized the wrong team! <br>Please login to the <a target="_blank" href="https://piraya.slack.com">Piraya Party Providers</a> team and authorize that team.'
    	})
    	
    }else{
    	console.log(req.account)
    	req.session['name'] = req.account.displayName
	    req.session['uid'] = req.account.id
	    res.redirect('/user/bing')
	}
    
  });

	
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});


